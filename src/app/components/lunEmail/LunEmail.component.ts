import {Component, Host, OnInit, SkipSelf} from '@angular/core';
import {FormGroupDirective} from "@angular/forms";
import {LunInputComponent} from "../lunInput/LunInput.component";

@Component({
  selector: 'lun-email',
  templateUrl: './LunEmail.component.html',
  styleUrls: ['./LunEmail.component.scss']
})
export class LunEmailComponent extends LunInputComponent implements OnInit {
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(
    @Host() @SkipSelf()
    formControlContainer: FormGroupDirective
  ) {
    super(formControlContainer);
  }

  ngOnInit() {
    super.ngOnInit();
  }


  getEmailErrorMessage() {
    return this.componentFormControl.hasError('required') ? 'Adres e-mail jest wymagany' :
      this.componentFormControl.hasError('email') ? 'Niepoprawny format adresu e-mail' :
        '';
  }
}
