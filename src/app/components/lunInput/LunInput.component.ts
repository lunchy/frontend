import {Component, Host, Input, OnInit, SkipSelf} from '@angular/core';
import {FormControl, FormGroupDirective, Validators} from "@angular/forms";

@Component({
  selector: 'lun-input',
  templateUrl: './LunInput.component.html',
  styleUrls: ['./LunInput.component.scss']
})
export class LunInputComponent implements OnInit {
  @Input('name') name: string;
  @Input('type') type: string;
  @Input('placeholder') placeholder: string;
  @Input('inputClasses') inputClasses: string[];
  @Input('wrapperClasses') wrapperClasses: string[];
  @Input('min') minLength: number;
  @Input('required') required: boolean;

  protected formControlContainer: FormGroupDirective;
  componentFormControl: FormControl;
  componentValidators = [];
  defaultComponentClasses = ['uk-input'];
  defaultComponentWrapperClasses = ['uk-margin'];

  constructor(
    @Host() @SkipSelf()
    formControlContainer: FormGroupDirective
  ) {
    this.formControlContainer = formControlContainer;
  }

  ngOnInit() {
    if (this.inputClasses && this.inputClasses.length) {
      this.defaultComponentClasses = this.defaultComponentClasses.concat(this.inputClasses);
    }

    if (this.wrapperClasses && this.wrapperClasses.length) {
      this.defaultComponentWrapperClasses = this.defaultComponentWrapperClasses.concat(this.wrapperClasses);
    }

    if (this.minLength) {
      this.componentValidators.push(Validators.minLength(+this.minLength));
    }

    if (this.required) {
      this.componentValidators.push(Validators.required);
    }

    this.componentFormControl = new FormControl('');
    this.componentFormControl.setValidators(this.componentValidators);
    this.formControlContainer.form.addControl(this.name, this.componentFormControl);
  }
}
