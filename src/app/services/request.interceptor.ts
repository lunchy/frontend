import 'rxjs/add/operator/do';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {ConfigService} from "./config.service";

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(private router: Router, private configService: ConfigService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      withCredentials: true
    });

    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        this.configService.capturedHttpError.next(err);
        if (err.status === 401) {
          this.router.navigate(['/login']);
          // or show a modal
        }
      }
    });
  }
}
