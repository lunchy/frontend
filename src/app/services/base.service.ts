import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class BaseHttpService {

  constructor(private http: HttpClient) {}

  get(url, options?) {
    return new Promise<any>((resolve, reject) => {
      return this.http.get(url, options)
        .subscribe(res => {
          return resolve(res);
        }, err => {
          return reject(err.message || 'Unknown error');
        });
    });
  }

  put(url, body?, options?) {
    return new Promise<any>((resolve, reject) => {
      return this.http.put(url, body, options)
        .subscribe(res => {
          return resolve(res);
        }, err => {
          return reject(err.message || 'Unknown error');
        });
    });
  }

  post(url, body?, options?) {
    return new Promise<any>((resolve, reject) => {
      return this.http.post(url, body, options)
        .subscribe(res => {
          return resolve(res);
        }, err => {
          return reject(err.message || 'Unknown error');
        });
    });
  }

  delete(url, options?) {
    return new Promise<any>((resolve, reject) => {
      return this.http.delete(url, options)
        .subscribe(res => {
          return resolve(res);
        }, err => {
          return reject(err.message || 'Unknown error');
        });
    });
  }


}
