import {Injectable} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {LoginCredentials} from '../models/LoginCredentials.model';
import {ConfigService} from './config.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BaseHttpService} from "./base.service";

@Injectable()
export class AuthService {
  isUserLogged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loggedUser: any;

  constructor(private http: HttpClient,
              private ls: LocalStorageService,
              private config: ConfigService,
              private baseHttpService: BaseHttpService) {
  }

  login(auth: LoginCredentials) {
    const bodyParams = "username=" + auth.username + "&password=" + auth.password;
    const httpOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    return this.baseHttpService.post(`${this.config.URL_API_DEV}/login`, bodyParams, httpOptions).then((data) => {
      this.isUserLogged.next(true);
      return data;
    });
  }

  logout() {
    return this.baseHttpService.post(`${this.config.URL_API_DEV}/logout`).then((data) => {
      this.logoutAndRemoveLocalData();
      return data;
    });
  }

  registerCompany(registerCompanyWrapper: any) {
    return this.baseHttpService.post(`${this.config.URL_API_DEV}/user/register/company/admin`, registerCompanyWrapper).then((data) => {
      return data;
    });
  }

  getLoggedUserDetails() {
    return this.baseHttpService.get(`${this.config.URL_API_DEV}/user/logged/details`).then((data) => {
      this.isUserLogged.next(true);
      return data;
    });
  }

  logoutAndRemoveLocalData() {
    this.isUserLogged.next(false);
    this.loggedUser = null;
  }
}
