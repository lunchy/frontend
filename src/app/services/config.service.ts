import {Injectable} from '@angular/core';
import {config} from '../app.config';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {HttpErrorResponse} from "@angular/common/http";

@Injectable()
export class ConfigService {

  capturedHttpError: BehaviorSubject<HttpErrorResponse> = new BehaviorSubject<HttpErrorResponse>(null);
  constructor() {
  }

  get URL_API_DEV() {
    return config.API.DEV;
  }
}
