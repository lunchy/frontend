import {Component, OnInit} from '@angular/core';
import {AuthService} from './services/auth.service';
import {Router} from "@angular/router";
import {ConfigService} from "./services/config.service";
import {HttpErrorResponse} from "@angular/common/http";
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateY(100%)', opacity: 0}),
          animate('1000ms', style({transform: 'translateY(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateY(0)', opacity: 1}),
          animate('1000ms', style({transform: 'translateY(100%)', opacity: 0}))
        ])
      ]
    )
  ]
})
export class AppComponent implements OnInit {
  sidebarItemList = [
    {name: 'Restauracje', state: ['/main'], icon: 'list'},
    {name: 'Zamówienia', state: ['/main'], icon: 'cart'},
    {name: 'Historia zamówień', state: ['/main'], icon: 'history'},
    {name: 'Ustawienia', state: ['/settings'], icon: 'settings'}
  ];
  userLoggedIn = false;
  errorCaptured: HttpErrorResponse;

  constructor(private authService: AuthService, private router: Router, private configService: ConfigService) {
    this.authService.isUserLogged.subscribe(isLogged => {
      this.userLoggedIn = isLogged;
    });
    this.configService.capturedHttpError.subscribe(error => {
      this.errorCaptured = error;
    });
  }

  ngOnInit() {
    this.authService.getLoggedUserDetails().then((loggedUserDetails) => {
      this.router.navigate(['/settings']);
    });
  }

  logout() {
    this.authService.logout().then((data) => {
      this.router.navigate(['/login']);
    })
  }

  hideErrorMessage() {
    this.errorCaptured = null;
  }

}
