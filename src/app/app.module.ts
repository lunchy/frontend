import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LocalStorageModule} from 'angular-2-local-storage';
import {appRoutes} from './router';

import {ConfigService} from './services/config.service';
import {AuthService} from './services/auth.service';

import {AppComponent} from './app.component';
import {LoginComponent} from './views/auth/Login/Login.component';
import {RegisterComponent} from './views/auth/Register/Register.component';
import {MainComponent} from './views/main/Main.component';
import {SettingsComponent} from './views/main/Settings/Settings.component';

import {AuthGuard} from './guards/AuthGuard';
import {RequestInterceptor} from "./services/request.interceptor";
import {BaseHttpService} from "./services/base.service";
import {LunInputComponent} from "./components/lunInput/LunInput.component";
import {LunEmailComponent} from "./components/lunEmail/LunEmail.component";


@NgModule({
  declarations: [AppComponent, LoginComponent, RegisterComponent, MainComponent, SettingsComponent, LunInputComponent, LunEmailComponent],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false} // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    LocalStorageModule.withConfig({
      prefix: 'lunchy-',
      storageType: 'localStorage'
    })
  ],
  providers: [

    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
    ConfigService,
    BaseHttpService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
