import {Routes} from '@angular/router';
import {RegisterComponent} from './views/auth/Register/Register.component';
import {LoginComponent} from './views/auth/Login/Login.component';
import {MainComponent} from './views/main/Main.component';
import {SettingsComponent} from './views/main/Settings/Settings.component';
import {AuthGuard} from './guards/AuthGuard';

export const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [AuthGuard]
  }
  // { path: '**', component: PageNotFoundComponent }
];
