import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {AdminWithCompanyRegistration} from '../../../models/AdminWithCompanyRegistration';
import {User} from "../../../models/User.model";
import {RegisterCompanyWrapper} from "../../../models/RegisterCompanyWrapper.model";

@Component({
  selector: 'lun-register',
  templateUrl: './Register.component.html',
  styleUrls: ['./Register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.registerFormGroup = this.formBuilder.group({
      ssn: ['', this.validatenip],
    });
  }

  onRegisterSubmit() {
    if (this.registerFormGroup.valid) {
      const registerAdmin = new User(this.registerFormGroup.value.firstName, this.registerFormGroup.value.lastName, this.registerFormGroup.value.email);
      const registerCompany = new RegisterCompanyWrapper(this.registerFormGroup.value.companyName, this.registerFormGroup.value.ssn);
      const registerWrapper = new AdminWithCompanyRegistration(registerAdmin, registerCompany);
      this.authService.registerCompany(registerWrapper.data).then((response) => {
        this.router.navigate(['/login']);
      });
    }
  }

  validatenip(formControl: FormControl) {
    const nip = formControl.value;
    const nipWithoutDashes = nip.replace(/-/g, '');
    const reg = /^[0-9]{10}$/;
    if (reg.test(nipWithoutDashes) === false) {
      return {invalidSSN: {value: true}};
    } else {
      const digits = ('' + nipWithoutDashes).split('');
      const checksum = (6 * parseInt(digits[0], 10) + 5 * parseInt(digits[1], 10) + 7 * parseInt(digits[2], 10) +
        2 * parseInt(digits[3], 10) + 3 * parseInt(digits[4], 10) + 4 * parseInt(digits[5], 10) +
        5 * parseInt(digits[6], 10) + 6 * parseInt(digits[7], 10) + 7 * parseInt(digits[8], 10)) % 11;
      return (parseInt(digits[9], 10) === checksum) ? null : {invalidSSN: {value: (parseInt(digits[9], 10) === checksum)}};
    }
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }

}
