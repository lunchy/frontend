import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'settings',
  templateUrl: './Settings.component.html',
  styleUrls: ['./Settings.component.scss']
})
export class SettingsComponent implements OnInit {
  userDataForm: FormGroup;
  passwordForm: FormGroup;

  constructor(private authService: AuthService, private formBuilder: FormBuilder) {
    this.authService.isUserLogged.next(true);
  }

  ngOnInit() {
    this.userDataForm = this.formBuilder.group({});
    this.passwordForm = this.formBuilder.group({});
  }
}
