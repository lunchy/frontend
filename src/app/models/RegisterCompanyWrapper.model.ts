export class RegisterCompanyWrapper {
  name: string;
  ssn: string;

  constructor(name: string, ssn: string) {
    this.name = name;
    this.ssn = ssn;
  }
}
