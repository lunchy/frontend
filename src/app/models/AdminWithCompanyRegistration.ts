import {User} from './User.model';
import {RegisterCompanyWrapper} from './RegisterCompanyWrapper.model';

export class AdminWithCompanyRegistration {
  admin: User;
  company: RegisterCompanyWrapper;

  constructor(admin: User, company: RegisterCompanyWrapper) {
    this.admin = admin;
    this.company = company;
  }

  get data() {
    return {admin: this.admin, company: this.company};
  }
}
