import {Company} from './Company.model';

export class User {
  company: Company;
  email: string;
  firstLogin: boolean;
  firstName: string;
  id: number;
  lastName: string;
  phone: string;

  constructor(firstName: string, lastName: string, email: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
