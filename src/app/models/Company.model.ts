import {User} from './User.model';

export class Company {
  id: number;
  name: string;
  ssn: string;
  user: User;

  constructor(id: number, name: string, ssn: string, user: User) {
    this.id = id;
    this.name = name;
    this.ssn = ssn;
    this.user = user;
  }
}
